package com.newgfmisinterface.rpinterfaceepension.dto;

public class EpensionTextItemDto {
    private String lineType;
    private String postingKey;
    private String accountType;
    private String accountCode;
    private String businessArea;
    private String costCenter;
    private String fund;
    private String fundCenter;
    private String functionalArea;
    private String businessProcess;
    private String amount;
    private String depositReference;
    private String assignment;
    private String earmarkedFundNo;
    private String earmarkedFundItem;
    private String bank;
    private String gpsc;
    private String subAccount;
    private String subAccountOwner;
    private String paymentCenterDetail;
    private String depositOwner;
    private String deposit;
    private String text;
    private String paymentTerm;
    private String paymentMethod;
    private String WTType;
    private String WTCode;
    private String WTBaseAmount;
    private String WTAmount;
    private String WTTypeFine;
    private String WTCodeFine;
    private String WTBaseAmountFine;
    private String WTAmountFine;
    private String searchTermVendor;
    private String vendorDepositAC;
    private String partnerBank;
    private String companyIDTradingPartner;
    private String tradingBA;
    private String vendorAccName;

    public String getLineType() {
        return lineType;
    }

    public void setLineType(String lineType) {
        this.lineType = lineType;
    }

    public String getPostingKey() {
        return postingKey;
    }

    public void setPostingKey(String postingKey) {
        this.postingKey = postingKey;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountCode() {
        return accountCode;
    }

    public void setAccountCode(String accountCode) {
        this.accountCode = accountCode;
    }

    public String getBusinessArea() {
        return businessArea;
    }

    public void setBusinessArea(String businessArea) {
        this.businessArea = businessArea;
    }

    public String getCostCenter() {
        return costCenter;
    }

    public void setCostCenter(String costCenter) {
        this.costCenter = costCenter;
    }

    public String getFund() {
        return fund;
    }

    public void setFund(String fund) {
        this.fund = fund;
    }

    public String getFundCenter() {
        return fundCenter;
    }

    public void setFundCenter(String fundCenter) {
        this.fundCenter = fundCenter;
    }

    public String getFunctionalArea() {
        return functionalArea;
    }

    public void setFunctionalArea(String functionalArea) {
        this.functionalArea = functionalArea;
    }

    public String getBusinessProcess() {
        return businessProcess;
    }

    public void setBusinessProcess(String businessProcess) {
        this.businessProcess = businessProcess;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDepositReference() {
        return depositReference;
    }

    public void setDepositReference(String depositReference) {
        this.depositReference = depositReference;
    }

    public String getAssignment() {
        return assignment;
    }

    public void setAssignment(String assignment) {
        this.assignment = assignment;
    }

    public String getEarmarkedFundNo() {
        return earmarkedFundNo;
    }

    public void setEarmarkedFundNo(String earmarkedFundNo) {
        this.earmarkedFundNo = earmarkedFundNo;
    }

    public String getEarmarkedFundItem() {
        return earmarkedFundItem;
    }

    public void setEarmarkedFundItem(String earmarkedFundItem) {
        this.earmarkedFundItem = earmarkedFundItem;
    }

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getGpsc() {
        return gpsc;
    }

    public void setGpsc(String gpsc) {
        this.gpsc = gpsc;
    }

    public String getSubAccount() {
        return subAccount;
    }

    public void setSubAccount(String subAccount) {
        this.subAccount = subAccount;
    }

    public String getSubAccountOwner() {
        return subAccountOwner;
    }

    public void setSubAccountOwner(String subAccountOwner) {
        this.subAccountOwner = subAccountOwner;
    }

    public String getPaymentCenterDetail() {
        return paymentCenterDetail;
    }

    public void setPaymentCenterDetail(String paymentCenterDetail) {
        this.paymentCenterDetail = paymentCenterDetail;
    }

    public String getDepositOwner() {
        return depositOwner;
    }

    public void setDepositOwner(String depositOwner) {
        this.depositOwner = depositOwner;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getPaymentTerm() {
        return paymentTerm;
    }

    public void setPaymentTerm(String paymentTerm) {
        this.paymentTerm = paymentTerm;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getWTType() {
        return WTType;
    }

    public void setWTType(String WTType) {
        this.WTType = WTType;
    }

    public String getWTCode() {
        return WTCode;
    }

    public void setWTCode(String WTCode) {
        this.WTCode = WTCode;
    }

    public String getWTBaseAmount() {
        return WTBaseAmount;
    }

    public void setWTBaseAmount(String WTBaseAmount) {
        this.WTBaseAmount = WTBaseAmount;
    }

    public String getWTAmount() {
        return WTAmount;
    }

    public void setWTAmount(String WTAmount) {
        this.WTAmount = WTAmount;
    }

    public String getWTTypeFine() {
        return WTTypeFine;
    }

    public void setWTTypeFine(String WTTypeFine) {
        this.WTTypeFine = WTTypeFine;
    }

    public String getWTCodeFine() {
        return WTCodeFine;
    }

    public void setWTCodeFine(String WTCodeFine) {
        this.WTCodeFine = WTCodeFine;
    }

    public String getWTBaseAmountFine() {
        return WTBaseAmountFine;
    }

    public void setWTBaseAmountFine(String WTBaseAmountFine) {
        this.WTBaseAmountFine = WTBaseAmountFine;
    }

    public String getWTAmountFine() {
        return WTAmountFine;
    }

    public void setWTAmountFine(String WTAmountFine) {
        this.WTAmountFine = WTAmountFine;
    }

    public String getSearchTermVendor() {
        return searchTermVendor;
    }

    public void setSearchTermVendor(String searchTermVendor) {
        this.searchTermVendor = searchTermVendor;
    }

    public String getVendorDepositAC() {
        return vendorDepositAC;
    }

    public void setVendorDepositAC(String vendorDepositAC) {
        this.vendorDepositAC = vendorDepositAC;
    }

    public String getPartnerBank() {
        return partnerBank;
    }

    public void setPartnerBank(String partnerBank) {
        this.partnerBank = partnerBank;
    }

    public String getCompanyIDTradingPartner() {
        return companyIDTradingPartner;
    }

    public void setCompanyIDTradingPartner(String companyIDTradingPartner) {
        this.companyIDTradingPartner = companyIDTradingPartner;
    }

    public String getTradingBA() {
        return tradingBA;
    }

    public void setTradingBA(String tradingBA) {
        this.tradingBA = tradingBA;
    }

    public String getVendorAccName() {
        return vendorAccName;
    }

    public void setVendorAccName(String vendorAccName) {
        this.vendorAccName = vendorAccName;
    }
}
