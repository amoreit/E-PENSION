package com.newgfmisinterface.rpinterfaceepension.dao;

import com.newgfmisinterface.rpinterfaceepension.dto.EpensionItemDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class EpensionItemDao {
    private static Logger LOGGER = LoggerFactory.getLogger(EpensionItemDao.class);
    @Autowired
    private JdbcTemplate jdbcTemplate;

    public int addItems(EpensionItemDto itemDto) throws Exception {
        String docDate = null;
        docDate = itemDto.getDocDate().equals("")? null: "to_timestamp('"+itemDto.getDocDate()+"','YYYYMMDD HH24:MI:SS.FF1')";
        String postedDate = null;
        postedDate = itemDto.getPostedDate().equals("")? null: "to_timestamp('"+itemDto.getPostedDate()+"','YYYYMMDD HH24:MI:SS.FF1')";
        String revDate = null;
        revDate = itemDto.getRevDate().equals("")? null: "to_timestamp('"+itemDto.getRevDate()+ "','YYYYMMDD HH24:MI:SS.FF1')";
        String sql = "INSERT INTO TH_RP_TRN_EPENSION_ITEM (FILE_NAME,LINE_TYPE,DOC_TYPE,AGENCY_CODE,DOC_DATE," +
                "POSTED_DATE,REF_NO,CURRENCY,REV_DATE,REV_REASON," +
                "PAYMENT_CENTER,ITEMNO,POSTING_KEY,ACC_TYPE,ACC_CODE," +
                "BUSINESS_AREA,CCT,FUND_SOURCE,FUND_CENTER,FUNCTIONAL_AREA," +
                "BUSINESS_PROCESS,AMOUNT,DEP_REF,ASSIGNMENT,EARMARKED_FUND_NO," +
                "EARMARKED_FUND_ITEM,BANK,GPSC,SUB_ACC,SUB_ACC_OWNER," +
                "PAYMENT_CENTER_DETAIL,DEP_OWNER,DEP_ACC,DESC_1,PAYMENT_TERM," +
                "PAYMENT_METHOD,WIT_TYPE,WTH_CODE,WTH_BASE_AMT,WTH_AMT," +
                "SEARCH_TERM_VENDOR,VENDOR_DEP_ACC,VENDOR_BANK,VENDOR_ACC_NAME,COMPANY_ID," +
                "TRADING_BA,DOC_NO,UNAME,CREATED_DATE,CREATED_PAGE," +
                "CREATED_USER,IPADDR,UPDATED_DATE,UPDATED_PAGE,UPDATED_USER," +
                "RECORD_STATUS,ISACTIVE)" +
                "values ('" + itemDto.getFileName() + "','" + itemDto.getLineType() + "','" + itemDto.getDocType() + "','" + itemDto.getAgencyCode() + "'," + docDate+ "," +
                " " + postedDate+ ",'" + itemDto.getRefNo()+ "','" + itemDto.getCurrency()+ "'," + revDate + ",'" + itemDto.getRevReason() + "'," +
                "'" + itemDto.getPaymentCenter() + "','" + itemDto.getItemNo() + "','" + itemDto.getPostingKey()+ "','" + itemDto.getAccType()+ "','" + itemDto.getAccCode() + "'," +
                "'" + itemDto.getBusinessArea() + "','" + itemDto.getCct() + "','" + itemDto.getFundSource() + "','" + itemDto.getFundCenter()+ "','" + itemDto.getFunctionalArea() + "'," +
                "'" + itemDto.getBusinessProcess() + "','" + itemDto.getAmount()+ "','" + itemDto.getDepRef() + "','" + itemDto.getAssignment() + "','" + itemDto.getEarmarkedFundNo() + "'," +
                "'" + itemDto.getEarmarkedFundItem()+ "','" + itemDto.getBank() + "','" + itemDto.getGpsc()+ "','" + itemDto.getSubAcc()+ "','" + itemDto.getSubAccOwner() + "'," +
                "'" + itemDto.getPaymentCenterDetail() + "','" + itemDto.getDepOwner() + "','" + itemDto.getDepAcc() + "','" + itemDto.getDesc1() + "','" + itemDto.getPaymentTerm() + "'," +
                "'" + itemDto.getPaymentMethod()+ "','" + itemDto.getWitType() + "','" + itemDto.getWthCode() + "','" + itemDto.getWthBaseAmt() + "','" + itemDto.getWthAmt() + "'," +
                "'" + itemDto.getSearchTermVendor() + "','" + itemDto.getVendorDepAcc() + "','" + itemDto.getVendorBank()+ "','" + itemDto.getVendorAccName() + "','" + itemDto.getCompanyId()+ "'," +
                "'" + itemDto.getTradingBa() + "','" + itemDto.getDocNo() + "','" + itemDto.getUname() + "','" + itemDto.getCreatedDate() + "','" + itemDto.getCreatedPage() + "'," +
                "'" + itemDto.getCreatedUser() + "','" + itemDto.getIpAddr() + "','" + itemDto.getUpdatedDate() + "','" + itemDto.getUpdatedPage()+ "','" + itemDto.getUpdatedUser() + "'," +
                "'" + itemDto.getRecordStatus() + "','" + itemDto.getIsactive()+ "')";
System.out.println("file => " + sql);
        return jdbcTemplate.update(sql);
    }
}
