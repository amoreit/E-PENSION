package com.newgfmisinterface.rpinterfaceepension.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class SystemConfigDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public String getInputFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='INCOMING_PATH_EPANSION_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getOutputFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='OUTGOING_PATH_EPENSION_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getUploadFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='UPLOAD_PATH_EPENSION_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getSuccessFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='SUCCESS_PATH_EPENSION_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getErrorFileConfig()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='ERROR_PATH_EPENSION_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }
    public String getSchedulePeriodD()  throws Exception  {
        String query = "SELECT VALUE FROM SYS_PARAMETERS_CONFIG WHERE PARAMETER_NAME='SCHEDULE_PERIODD_EPANSION_BATCHJOB'";
        return jdbcTemplate.queryForObject(query,new Object[]{},String.class);
    }

}
