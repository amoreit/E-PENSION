package com.newgfmisinterface.rpinterfaceepension.controller;

import com.newgfmisinterface.rpinterfaceepension.dao.EpensionSendPathDao;
import com.newgfmisinterface.rpinterfaceepension.dto.RequestFrontDto;
import com.newgfmisinterface.rpinterfaceepension.dto.ResponseDto;
import com.newgfmisinterface.rpinterfaceepension.job.ProcessJobEPS;
import com.newgfmisinterface.rpinterfaceepension.service.FileStorageService;
import com.newgfmisinterface.rpinterfaceepension.service.ScanFilePathService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

@RestController
public class ExecuteTaskController {
    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ScanFilePathService scanFilePath;

    @Autowired
    private TaskScheduler taskScheduler;

    @Autowired
    private ProcessJobEPS processJobEPS;

    @Autowired
    private EpensionSendPathDao epensionSendPathDao;

    @GetMapping("/testEpayroll")
    public ResponseEntity<String> executeTask() {
        long startTimeTotal = System.currentTimeMillis();

        String[] projectFile = {"EE202003140933130011002EXCINFCT01.TXT"};
        List<String> projectName = new ArrayList<>();
        int lengthProject = projectFile.length;
        for (String projectCode : projectFile) {

            long startTime = System.currentTimeMillis();
            List<String> moveFile = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectCode);

            int fileListForScan = 0;
            for (String moveFileToProcess : moveFile) {
                File fileName = new File(moveFileToProcess);
                String getFileName = fileName.getName();
                projectName.add(getFileName);
                fileStorageService.moveFileToProcessDir(moveFileToProcess);
                fileListForScan++;
            }
            if (fileListForScan > 0) {
                List<String> fileList = scanFilePath.listFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectCode);
                for (String filename : fileList) {
                    processJobEPS.jobRunner(filename);
                }
            }
            lengthProject--;

            long endTime = System.currentTimeMillis();
            long duration = (endTime - startTime);
            System.out.print("Execution time is " + duration + " milli seconds\n");
        }

        long endTimeTotal = System.currentTimeMillis();
        long durationTotal = (endTimeTotal - startTimeTotal);
        System.out.print("Completed time is " + durationTotal + " milli seconds");

        String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        return new ResponseEntity<String>("<h1>Job Task e-bidding is Running at Time : " + timeStamp + "</h1>", HttpStatus.OK);
    }

    @GetMapping("/convertFileEpension")
    public ResponseDto convertFileEbidding(RequestFrontDto requestFrontDto) {
        System.out.println(requestFrontDto);
//        ArrayList<List<Map<String, Object>>> resultList = null;
        String projectFile = requestFrontDto.getFileName();
//        String projectFile = "EE202003140933130011002EXCINFCT01.TXT";
        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);

        String moveFileName = moveFile.toString();
        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
        if (moveFileName.equals(null) || moveFileName.isEmpty()) {
            return new ResponseDto(2, "fail", null);
        } else {
            boolean statusMove = fileStorageService.moveFileToConvert(moveFileName);
            if (statusMove) {
                List<String> fileList = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectFile);
//                String transDate = requestFrontDto.getTransDate();
                String fileName = fileList.toString();
                fileName = fileName.replaceAll("[\\[\\]]", "");
                try {
                    processJobEPS.jobRunner(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
//            if(resultList.size() > 0){
                return new ResponseDto(1, "success", null);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", null);
        }


    }
    @GetMapping("/testConvertFileEpension")
    public ResponseDto testConvertFileEpayroll(RequestFrontDto requestFrontDto) {
        System.out.println(requestFrontDto);
//        ArrayList<List<Map<String, Object>>> resultList = null;
        String projectFile = requestFrontDto.getFileName();
//        String projectFile = "EE202003140933130011002EXCINFCT01.TXT";
        List<String> moveFile = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileInputLocation().toString()), projectFile);

        String moveFileName = moveFile.toString();
        moveFileName = moveFileName.replaceAll("[\\[\\]]", "");
        if (moveFileName.equals(null) || moveFileName.isEmpty()) {
            return new ResponseDto(2, "fail", null);
        } else {
            boolean statusMove = fileStorageService.copyFileToConvert(moveFileName);
            if (statusMove) {
                List<String> fileList = scanFilePath.listConvertAndPostFilesForFolder(new File(fileStorageService.getFileStorageLocation().toString()), projectFile);
                String fileName = fileList.toString();
                fileName = fileName.replaceAll("[\\[\\]]", "");
                try {
                    return new ResponseDto(1, "success", null);
//                    processJobFI.jobRunner(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        try {
//            if(resultList.size() > 0){
            return new ResponseDto(1, "success", null);
//            }else {
//                return new ResponseDto(0, "fail", null);
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", null);
        }


    }

    @GetMapping("/ePensionPathUploadFile")
    public ResponseDto getEpayrollPathUploadFile() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = epensionSendPathDao.getEpensionPathUploadFile();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }
    @GetMapping("/ePensionPathOutputFile")
    public ResponseDto getEpayrollPathOutputFile() {

        List<Map<String, Object>> resultList = null;
        try {
            resultList = epensionSendPathDao.getEpensionPathOutputFile();
            System.out.println("<<< resultList: " + resultList);
            return new ResponseDto(1, "success", resultList);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseDto(0, "fail", resultList);
        }
    }
}
