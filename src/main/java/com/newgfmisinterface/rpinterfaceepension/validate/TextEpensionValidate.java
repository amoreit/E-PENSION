package com.newgfmisinterface.rpinterfaceepension.validate;

import com.newgfmisinterface.rpinterfaceepension.dto.EpensionControlLineDto;
import com.newgfmisinterface.rpinterfaceepension.dto.EpensionHeaderLineDto;
import com.newgfmisinterface.rpinterfaceepension.dto.EpensionItemDto;
import com.newgfmisinterface.rpinterfaceepension.dto.EpensionTextItemDto;

public class TextEpensionValidate {
    public TextEpensionValidate() {
    }
    public EpensionHeaderLineDto substringHeaderLineObj(String textLine) {

        EpensionHeaderLineDto headerLineDto = new EpensionHeaderLineDto();

        String lineType = textLine.substring(0, 1);
        String trimLineType = lineType.trim();
        headerLineDto.setLineType(trimLineType);

        String documentType = textLine.substring(1, 3);
        String trimDocumentType = documentType.trim();
        headerLineDto.setDocumentType(trimDocumentType);

        String companyCode = textLine.substring(3, 7);
        String trimCompanyCode = companyCode.trim();
        headerLineDto.setCompanyCode(trimCompanyCode);

        String documentDate = textLine.substring(7, 15);
        String trimDocumentDate = documentDate.trim();
        headerLineDto.setDocumentDate(trimDocumentDate);

        String postingDate = textLine.substring(15, 23);
        String trimPostingDate = postingDate.trim();
        headerLineDto.setPostingDate(trimPostingDate);

        String reference = textLine.substring(23, 39);
        String trimReference = reference.trim();
        headerLineDto.setReference(trimReference);

        String currency = textLine.substring(39, 44);
        String trimCurrency = currency.trim();
        headerLineDto.setCurrency(trimCurrency);

        String reverseDate = textLine.substring(44, 52);
        String trimReverseDate = reverseDate.trim();
        headerLineDto.setReverseDate(trimReverseDate);

        String reversalReason = textLine.substring(52, 54);
        String trimReversalReason = reversalReason.trim();
        headerLineDto.setReversalReason(trimReversalReason);

        String disbursementUnit = textLine.substring(54, 69);
        String trimDisbursementUnit = disbursementUnit.trim();
        headerLineDto.setPaymentCenterHeader(trimDisbursementUnit);

//        String excelFilename = textLine.substring(69, 147);
//        String trimExcelFilename = excelFilename.trim();
//        headerLineDto.setExcelFilename(trimExcelFilename);
//
//        String ownerUser = textLine.substring(147, 161);
//        String trimOwnerUser = ownerUser.trim();
//        headerLineDto.setOwnerUser(trimOwnerUser);

        return headerLineDto;
    }

    public EpensionTextItemDto substringItemLineObj(String textLine) {

        EpensionTextItemDto itemLineDto = new EpensionTextItemDto();

        String lineType = textLine.substring(0, 1);
        String trimLineType = lineType.trim();
        itemLineDto.setLineType(trimLineType);

        String postingKey = textLine.substring(1, 3);
        String trimPostingKey = postingKey.trim();
        itemLineDto.setPostingKey(trimPostingKey);

        String accountType = textLine.substring(3, 4);
        String trimAccountType = accountType.trim();
        itemLineDto.setAccountType(trimAccountType);

        String accountCode = textLine.substring(4, 14);
        String trimAccountCode = accountCode.trim();
        itemLineDto.setAccountCode(trimAccountCode);

        String businessArea = textLine.substring(14, 18);
        String trimBusinessArea = businessArea.trim();
        itemLineDto.setBusinessArea(trimBusinessArea);

        String costCenter = textLine.substring(18, 28);
        String trimCostCenter = costCenter.trim();
        itemLineDto.setCostCenter(trimCostCenter);

        String fund = textLine.substring(28, 38);
        String trimFund = fund.trim();
        itemLineDto.setFund(trimFund);

        String fundCenter = textLine.substring(38, 54);
        String trimFundCenter = fundCenter.trim();
        itemLineDto.setFundCenter(trimFundCenter);

        String functionalArea = textLine.substring(54, 70);
        String trimFunctionalArea = functionalArea.trim();
        itemLineDto.setFunctionalArea(trimFunctionalArea);

        String businessProcess = textLine.substring(70, 82);
        String trimBusinessProcess = businessProcess.trim();
        itemLineDto.setBusinessProcess(trimBusinessProcess);

        String amount = textLine.substring(82, 98);
        String trimAmount = amount.trim();
        itemLineDto.setAmount(trimAmount);

        String depositReference = textLine.substring(98, 118);
        String trimDepositReference = depositReference.trim();
        itemLineDto.setDepositReference(trimDepositReference);

        String assignment = textLine.substring(118, 136);
        String trimAssignment = assignment.trim();
        itemLineDto.setAssignment(trimAssignment);

        String earmarkedFundNo = textLine.substring(137, 146);
        String trimEarmarkedFundNo = earmarkedFundNo.trim();
        itemLineDto.setEarmarkedFundNo(trimEarmarkedFundNo);

        String earmarkedFundItem = textLine.substring(146, 149);
        String trimEarmarkedFundItem = earmarkedFundItem.trim();
        itemLineDto.setEarmarkedFundItem(trimEarmarkedFundItem);

        String bank = textLine.substring(149, 155);
        String trimBank = bank.trim();
        itemLineDto.setBank(trimBank);

        String gpsc = textLine.substring(155, 169);
        String trimGpsc = gpsc.trim();
        itemLineDto.setGpsc(trimGpsc);

        String subAccount = textLine.substring(169, 176);
        String trimSubAccount = subAccount.trim();
        itemLineDto.setSubAccount(trimSubAccount);

        String subAccountOwner = textLine.substring(176, 186);
        String trimSubAccountOwner = subAccountOwner.trim();
        itemLineDto.setSubAccountOwner(trimSubAccountOwner);

        String disbursementUnit = textLine.substring(186, 201);
        String trimDisbursementUnit = disbursementUnit.trim();
        itemLineDto.setPaymentCenterDetail(trimDisbursementUnit);

        String depositOwner = textLine.substring(201, 211);
        String trimDepositOwner = depositOwner.trim();
        itemLineDto.setDepositOwner(trimDepositOwner);

        String deposit = textLine.substring(211, 216);
        String trimDeposit = deposit.trim();
        itemLineDto.setDeposit(trimDeposit);

        String text = textLine.substring(216, 266);
        String trimText = text.trim();
        itemLineDto.setText(trimText);

        String paymentTerm = textLine.substring(266, 270);
        String trimPaymentTerm = paymentTerm.trim();
        itemLineDto.setPaymentTerm(trimPaymentTerm);

        String paymentMethod = textLine.substring(270, 271);
        String trimPaymentMethod = paymentMethod.trim();
        itemLineDto.setPaymentMethod(trimPaymentMethod);

        String wtType = textLine.substring(271, 273);
        String trimWtType = wtType.trim();
        itemLineDto.setWTType(trimWtType);

        String wtCode = textLine.substring(273, 275);
        String trimWtCode = wtCode.trim();
        itemLineDto.setWTCode(trimWtCode);

        String wtBaseAmount = textLine.substring(275, 291);
        String trimWtBaseAmount = wtBaseAmount.trim();
        itemLineDto.setWTBaseAmount(trimWtBaseAmount);

        String wtAmount = textLine.substring(291, 307);
        String trimWtAmount = wtAmount.trim();
        itemLineDto.setWTAmount(trimWtAmount);

        String wtTypeFine = textLine.substring(307, 309);
        String trimWtTypeFine = wtTypeFine.trim();
        itemLineDto.setWTTypeFine(trimWtTypeFine);

        String wtCodeFine = textLine.substring(309, 311);
        String trimWtCodeFine = wtCodeFine.trim();
        itemLineDto.setWTCodeFine(trimWtCodeFine);

        String wtBaseAmountFine = textLine.substring(311, 327);
        String trimWtBaseAmountFine = wtBaseAmountFine.trim();
        itemLineDto.setWTBaseAmountFine(trimWtBaseAmountFine);

        String wtAmountFine = textLine.substring(327, 343);
        String trimWtAmountFine = wtAmountFine.trim();
        itemLineDto.setWTAmountFine(trimWtAmountFine);

        String searchTermVendor = textLine.substring(343, 363);
        String trimSearchTermVendor = searchTermVendor.trim();
        itemLineDto.setSearchTermVendor(trimSearchTermVendor);

        String vendorDepositAC = textLine.substring(363, 381);
        String trimVendorDepositAC = vendorDepositAC.trim();
        itemLineDto.setVendorDepositAC(trimVendorDepositAC);

        String partnerBank = textLine.substring(381, 396);
        String trimPartnerBank = partnerBank.trim();
        itemLineDto.setPartnerBank(trimPartnerBank);

        String companyIDTradingPartner = textLine.substring(396, 402);
        String trimCompanyIDTradingPartner = companyIDTradingPartner.trim();
        itemLineDto.setCompanyIDTradingPartner(trimCompanyIDTradingPartner);

        String tradingBA = textLine.substring(402, 406);
        String trimTradingBA = tradingBA.trim();
        itemLineDto.setTradingBA(trimTradingBA);

        String vendorAccName = textLine.substring(406, 414);
        String trimVendorAccName = vendorAccName.trim();
        itemLineDto.setVendorAccName(trimVendorAccName);

        return itemLineDto;
    }

    public EpensionControlLineDto substringControlLineObj(String textLine) {

        EpensionControlLineDto controlLineDto = new EpensionControlLineDto();

        String lineType = textLine.substring(0, 1);
        String trimLineType = lineType.trim();
        controlLineDto.setLineType(trimLineType);

        String totalLineCount = textLine.substring(1, 7);
        String trimTotalLineCount = totalLineCount.trim();
        controlLineDto.setTotalLineCount(trimTotalLineCount);

        String zipFilename = textLine.substring(7, 87);
        String trimZipFilename = zipFilename.trim();
        controlLineDto.setZipFilename(trimZipFilename);

        return controlLineDto;
    }
}
