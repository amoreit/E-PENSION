package com.newgfmisinterface.rpinterfaceepension.service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;

import com.newgfmisinterface.rpinterfaceepension.dao.SystemConfigDao;
import com.newgfmisinterface.rpinterfaceepension.properties.FileStorageProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileStorageService {
    private static Logger LOGGER = LoggerFactory.getLogger(FileStorageService.class);
    private final Path fileStorageLocation;
    private final Path fileSuccessLocation;
    private final Path fileOutputLocation;
    private final Path fileInputLocation;
//    private final Path fileExtractLocation;
    private final Path fileErrorLocation;

    @Autowired
    private SystemConfigDao systemConfigDao;

    @Autowired
    public FileStorageService(FileStorageProperties fileStorageProperties, SystemConfigDao systemConfigDao) {
        this.systemConfigDao = systemConfigDao;

        String inputPath = fileStorageProperties.getInputDir();
        try {
            inputPath = this.systemConfigDao.getInputFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String outputPath = fileStorageProperties.getOutputDir();
        try {
            outputPath = this.systemConfigDao.getOutputFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String uploadPath = fileStorageProperties.getUploadDir();
        try {
            uploadPath = this.systemConfigDao.getUploadFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        String extractPath = fileStorageProperties.getExtractDir();
//        try {
//            extractPath = this.systemConfigDao.getExtractFileConfig();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        String successPath = fileStorageProperties.getSuccessDir();
        try {
            successPath = this.systemConfigDao.getSuccessFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }
        String errorPath = fileStorageProperties.getErrorDir();
        try {
            errorPath = this.systemConfigDao.getErrorFileConfig();
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.fileStorageLocation = Paths.get(uploadPath).toAbsolutePath().normalize();
        this.fileSuccessLocation = Paths.get(successPath).toAbsolutePath().normalize();
        this.fileOutputLocation = Paths.get(outputPath).toAbsolutePath().normalize();
//        this.fileExtractLocation = Paths.get(extractPath).toAbsolutePath().normalize();
        this.fileInputLocation = Paths.get(inputPath).toAbsolutePath().normalize();
        this.fileErrorLocation = Paths.get(errorPath).toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
            Files.createDirectories(this.fileSuccessLocation);
            Files.createDirectories(this.fileOutputLocation);
//            Files.createDirectories(this.fileExtractLocation);
            Files.createDirectories(this.fileInputLocation);
            Files.createDirectories(this.fileErrorLocation);
        } catch (Exception ex) {
            System.out.println(ex);
            LOGGER.error("createDirectories " + ex);
        }
    }
//    public void deleteFileSuccess(String filename) {
//        String subsFileName = filename.substring(0, 21);
//        String newName = subsFileName + "_OUT.TXT";
//        File fileName = new File(this.fileOutputLocation.toString() + "/" + newName);
//        String filepath = fileName.getAbsolutePath();
//
//        try {
//            File file = new File(String.valueOf(Paths.get(filepath)));
//System.out.println("Paths>>>>>>>>>>"+file);
//            if(file.delete()) {
//                System.out.println("Delete success");
//            }else {
//                System.out.println("Delete failed");
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//    public void fileOutPass(String fileName, String date, String type, String status) {
//        String subsFileName = fileName.substring(0, 21);
//        String newName = subsFileName + "_OUT.TXT";
//        File filePath = new File(this.fileOutputLocation.toString() + "/" + newName);
//        String filePathName = filePath.getAbsolutePath();
//
//        File fileNameChange = new File(filePathName);
//        FileWriter writer;
//        try {
//            writer = new FileWriter(fileNameChange, true);
//            writer.write(fileName + "|" + date + "|" + type + "|" + status + " \n");
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public void textFileError(EbiddingErrorDto dto, String dateString) {
//        String fileName = dto.getFilename();
//        String subsFileName = fileName.substring(0, 21);
//        String newName = subsFileName + "_OUT.TXT";
//        File filePath = new File(this.fileOutputLocation.toString() + "/" + newName);
//        String filePathName = filePath.getAbsolutePath();
//        File filename = new File(filePathName);
//        FileWriter writer;
//        try {
//            writer = new FileWriter(filename, true);
//            writer.write(dto.getFilename() + "|" + dateString + "|" + dto.getDoctype() + "|" + dto.getItemno() + "|" + dto.getMessage() + " \n");
//            writer.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

    public void MoveFiletoSuccessDirDate(String filename) {
        String date = filename.substring(2, 10);
        File dirFileSuccess = new File(this.fileSuccessLocation.toString() + "/" + date);
        dirFileSuccess.mkdir();

        File fileupload = new File(this.fileStorageLocation.toString() + "/" + filename);
        String fileuploadfullpath = fileupload.getAbsolutePath();

        File fileSuccess = new File(this.fileSuccessLocation.toString() + "/" + date + "/" + filename);
        String fileSuccessfullpath = fileSuccess.getAbsolutePath();

        try {
            Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("MoveFiletoSuccessDir " + e);
        }
    }

//    public void MoveFileToErrorDirDate(String filename) {
//        String dateTime = new SimpleDateFormat("HHmmss").format(new Date());
//        String date = filename.substring(13, 21);
//        File dirFileError = new File(this.fileErrorLocation.toString() + "/" + date);
//        dirFileError.mkdir();
//
//        File fileupload = new File(this.fileStorageLocation.toString() + "/" + filename);
//        String fileuploadfullpath = fileupload.getAbsolutePath();
//
//
//        String subsFileName = filename.substring(0, 21);
//        String newName = subsFileName + "_T" + dateTime + ".TXT";
//
//        File newname = new File(this.fileStorageLocation.toString() + "/" + newName);
//        String filePathChangeName = newname.getAbsolutePath();
//
//        File oldNameFile = new File(fileuploadfullpath);
//        File newNameFile = new File(filePathChangeName);
//        String fileNameNew = null;
//        if (oldNameFile.renameTo(newNameFile)) {
//            fileNameNew = newNameFile.getName();
//        } else {
//            System.out.println("Rename failed");
//        }
//
//        File fileError = new File(this.fileErrorLocation.toString() + "/" + date + "/" + fileNameNew);
//        String fileErrorfullpath = fileError.getAbsolutePath();
//
//        try {
//            Files.move(Paths.get(filePathChangeName), Paths.get(fileErrorfullpath), StandardCopyOption.REPLACE_EXISTING);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public void MoveFileStatementSuccessDirDate(String filename) {
//        String date = filename.substring(12, 20);
//        File dirFileSuccess = new File(this.fileSuccessLocation.toString() + "/" + date);
//        dirFileSuccess.mkdir();
//
//        File fileupload = new File(this.fileStorageLocation.toString() + "/" + filename);
//        String fileuploadfullpath = fileupload.getAbsolutePath();
//
//        File fileSuccess = new File(this.fileSuccessLocation.toString() + "/" + date + "/" + filename);
//        String fileSuccessfullpath = fileSuccess.getAbsolutePath();
//
//        try {
//            Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
//        } catch (IOException e) {
//            e.printStackTrace();
//            LOGGER.error("MoveFiletoSuccessDir " + e);
//        }
//    }

//    public void MoveFiletoSuccessDir(String filename) {
//
//        File fileupload = new File(this.fileStorageLocation.toString() + "/" + filename);
//        String fileuploadfullpath = fileupload.getAbsolutePath();
//
//        File fileSuccess = new File(this.fileSuccessLocation.toString() + "/" + filename);
//        String fileSuccessfullpath = fileSuccess.getAbsolutePath();
//
//        try {
//            Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
//        } catch (IOException e) {
//            e.printStackTrace();
//            LOGGER.error("MoveFiletoSuccessDir " + e);
//        }
//    }

    public void moveFileToProcessDir(String filename) {

        File fileupload = new File(this.fileInputLocation.toString() + "/" + filename);
        String fileuploadfullpath = fileupload.getAbsolutePath();

        File fileSuccess = new File(this.fileStorageLocation.toString() + "/" + filename);
        String fileSuccessfullpath = fileSuccess.getAbsolutePath();

        try {
            Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("MoveFiletoSuccessDir " + e);
        }
    }
    public boolean moveFileToConvert(String filename) {

        File fileupload = new File(this.fileInputLocation.toString() + "/" + filename);
        String fileuploadfullpath = fileupload.getAbsolutePath();

        File fileSuccess = new File(this.fileStorageLocation.toString() + "/" + filename);
        String fileSuccessfullpath = fileSuccess.getAbsolutePath();

        try {
            Path temp = Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
            if(temp != null){
                return true;
            }else{
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("MoveFiletoSuccessDir " + e);
        }
        return false;
    }
    public boolean copyFileToConvert(String filename) {

        File fileupload = new File(this.fileInputLocation.toString() + "/" + filename);
        String fileuploadfullpath = fileupload.getAbsolutePath();


        File fileSuccess = new File(this.fileStorageLocation.toString() + "/" + filename);
        String fileSuccessfullpath = fileSuccess.getAbsolutePath();

        try {
            Path temp =  Files.copy(Paths.get(fileuploadfullpath),Paths.get(fileSuccessfullpath),StandardCopyOption.COPY_ATTRIBUTES,StandardCopyOption.REPLACE_EXISTING);

//            Path temp = Files.move(Paths.get(fileuploadfullpath), Paths.get(fileSuccessfullpath), StandardCopyOption.REPLACE_EXISTING);
            if(temp != null){
                return true;
            }else{
                return false;
            }
        } catch (IOException e) {
            e.printStackTrace();
            LOGGER.error("CopyFiletoSuccessDir " + e);
        }
        return false;
    }
//    public void deleteFileToProcessDir(String filename) {
//
//        File fileName = new File(this.fileStorageLocation.toString() + "/" + filename);
//        String filepath = fileName.getAbsolutePath();
//
//        try {
//            Files.delete(Paths.get(filepath));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }

//    public List<String> extractFileLine(final String filename) {
//
//        File file = new File(this.fileStorageLocation.toString() + "/" + filename);
//        String filenameFullPath = file.getAbsolutePath();
//        List<String> fileByline = this.readFilebyLine(filenameFullPath);
//
//        int numFileSplit = 0;
//        boolean isWriting = false;
//        int countLine = 0;
//
//        List<String> fileTempList = new ArrayList<>();
//        PrintWriter writer = null;
//        for (String line : fileByline) {
//            String[] headerlineParse = line.split("\\|");
//
//            String startfield = headerlineParse[0];
//            String dateCode = filename.substring(13, 21);
//            String checkType = line.substring(2, 4);
//            if (startfield.equals("H") && line.length() == 13) {
//                if (isWriting) {
//                    writer.close();
//                }
//                try {
//                    String newFileName = "GFEBIDING" + checkType + dateCode + ".TXT";
//                    File filetemp = new File(this.fileExtractLocation.toString() + "/" + newFileName);
//                    String filetempnamefullpath = filetemp.getAbsolutePath();
//                    fileTempList.add(filetempnamefullpath);
//                    writer = new PrintWriter(filetempnamefullpath, "UTF-8");
//                    isWriting = true;
//                    numFileSplit = numFileSplit + 1;
//                } catch (FileNotFoundException | UnsupportedEncodingException e) {
//                    isWriting = false;
//                    e.printStackTrace();
//                }
//            }
//            if (isWriting) {
//                writer.println(line);
//            }
//            countLine++;
//            if (isWriting && (fileByline.size() == countLine)) {
//                writer.close();
//            }
//        }
//        fileByline.clear();
//
//        return fileTempList;
//
//    }

    public List<String> readFilebyLine(final String filename) {

        ArrayList<String> result = new ArrayList<>();

        try {
            File fileDir = new File(filename);

            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(fileDir), "UTF8"));

            String str;

            while ((str = in.readLine()) != null) {
                result.add(str);
            }

            in.close();
            return result;

        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
            LOGGER.error("UnsupportedEncodingException " + e);
            return result;
        } catch (IOException e) {
            System.out.println(e.getMessage());
            LOGGER.error("IOException " + e);
            return result;

        } catch (Exception e) {
            System.out.println(e.getMessage());
            LOGGER.error("Exception " + e);
            return result;
        }
    }

    public Path getFileStorageLocation() {
        return fileStorageLocation;
    }

    public Path getFileSuccessLocation() {
        return fileSuccessLocation;
    }

    public Path getFileOutputLocation() {
        return fileOutputLocation;
    }

//    public Path getFileExtractLocation() {
//        return fileExtractLocation;
//    }

    public Path getFileInputLocation() {
        return fileInputLocation;
    }
}
